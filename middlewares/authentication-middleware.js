var Promise = require('bluebird');
var jwt = require('jsonwebtoken');

module.exports = (config) => {
	var router = require('express').Router();
	router.use(authenticate);
	config.app.use('/api', router);
}

function authenticate(req, res, next) {

	var token = req.headers['token'];

	if (!token) return res.status(401).json({success: false, message: 'No token found'});

	jwt.verify(token, app.get('secret'), function(err, decoded) {
		if (err) return res.status(401).json({success: false, message: 'Failed to authenticate token!'});    
		req.decoded = decoded;
		next();
	});
}
