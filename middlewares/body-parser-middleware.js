module.exports = (config) => {
	var bodyParser = require('body-parser');
	config.app.use(bodyParser.json());
	config.app.use(bodyParser.urlencoded({
  		extended: true
	})); 	
}