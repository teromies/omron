var jwt = require('jsonwebtoken');
var User = require('../models/user-model.js');

module.exports = (config) => {
	var router = require('express').Router();
	config.app.use('/login', router);
	router.post('/', login);
}

function login(req, res) {

	if (!req.body || !req.body.username) {
		return res.sendStatus(401);
	}

	User.findOne({username: req.body.username}).exec()
		.then((user) => {
			if (!user) return res.status(401).json({success: false, message: 'Wrong username!'});
			
			if (user.password != req.body.password) return res.status(401).json({success: false, message: 'Wrong password!'});

	        var token = jwt.sign(user, app.get('secret'), {
	        	expiresIn: 3600 
	        });
			res.json({
				success: true,
				message: 'Token created!',
				token: token
			});
		})
		.catch((err) => {
			console.error(err);
			res.json('Login failed!');
		})

}