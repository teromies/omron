
var Promise = require('bluebird');
var User = require('../models/user-model.js');

module.exports = (config) => {
	var router = require('express').Router();
	config.app.use('/user', router);

	router.get('/', getUsers);
	router.get('/:id', getUserById);

	router.post('/', createUser);
	router.delete('/:id', deleteUser);
}


function getUsers(req, res) {
	User.find().exec()
		.then((users) => { res.send(users);	})
		.catch((err) => { res.sendStatus(500); });
}

function getUserById(req, res) {
	User.findById(req.params.id).exec()
		.then((user) => { res.send(user) })
		.catch((err) => { res.sendStatus(500) });
}

function createUser(req, res) {

	var user = User({
	  	username: 'username',
  		password: 'password'
	});

	user.save()
		.then((created) => { res.send(created);	})
		.catch((err) => { res.sendStatus(500) });
}

function deleteUser(req, res) {
	User.findByIdAndRemove(req.params.id).exec()
		.then((user) => { res.send(user) })
		.catch((err) => { res.sendStatus(500) });
}


