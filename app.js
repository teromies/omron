const express = require('express');
const path = require('path');
const app = express();

const CONST = require('./config/constants.js');

app.set('secret', CONST.SECRET);

var config = {
	app: app
}

intializeMiddlewares()
	.then(initializeControllers)
	.then(setDefaultResponse)
	.then(connectInteruptSignal)
	.then(startServer)
	.catch((err) => {
		console.error(err);
	});

function initializeControllers() {
	return require('./lib/javascript-loader.js')(__dirname + '/controllers/', config);
}

function intializeMiddlewares() {
	return require('./lib/javascript-loader.js')(__dirname + '/middlewares/', config)
}

function setDefaultResponse() {
	app.use('/', (req, res) => {
		res.status(404).send('Nothing to see here!');
	});
}

function connectInteruptSignal() {
	process.on('SIGINT', () => {
		console.log('\nCaught interrupt signal\n');
	    process.exit();
	});
}

function startServer()  {
	return require('./lib/server')(config);
}

module.exports = app;