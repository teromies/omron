const http = require('http');
const CONST = require('../config/constants.js');

module.exports = (config) => {
    var server = config.app.listen(CONST.SERVER_PORT, CONST.SERVER_HOST, () => {
        console.log('\n############################################');
        console.log('Server started at http://%s:%s', server.address().address, server.address().port);
        console.log('############################################\n');
    });
}