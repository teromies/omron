const Promise = require('bluebird');
const fs = require('fs-extra');
const path = require('path');

module.exports = (dir, config) => {
	return Promise.map(fs.readdirSync(dir), (file) =>  {
		if (path.extname(file) == '.js') {
			require(dir + file)(config);
		}
	});
}