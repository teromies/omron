
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const CONST = require('../config/constants.js');

mongoose.connect(CONST.MONGO_ADDRESS);
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error : '));
db.once('open', function(){
	console.log('\nMongo connection opened to', CONST.MONGO_ADDRESS, '\n');
});

module.exports = mongoose;