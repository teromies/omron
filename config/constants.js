const SERVER_HOST = 'localhost';
const SERVER_PORT = 3000;
const SECRET = 'testisecretti';

var IS_TEST_ENV = process.env.NODE_ENV == 'test';
var MONGO_ADDRESS = IS_TEST_ENV ? 'mongodb://localhost/omron_test' : 'mongodb://localhost/omron';

module.exports = {
	SERVER_HOST: SERVER_HOST,
	SERVER_PORT: SERVER_PORT,
	SECRET: SECRET,
	MONGO_ADDRESS: MONGO_ADDRESS
}