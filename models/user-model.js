var mongoose = require('../lib/database.js');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	username: {type: String, required: true, unique: true},
	password: {type: String, required: true},
	firstname: String,
	lastname: String,
	created_at: Date,
	updated_at: Date
});

module.exports = mongoose.model('User', userSchema);