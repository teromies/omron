
var assert = require('chai').assert;
var User = require('../models/user-model.js');

describe('User model', function() {

	const TEST_USER_NAME = 'testusername';

	after(function() {
		return User.findOneAndRemove({ username: TEST_USER_NAME }).exec()
	});

	it('should create an user successfully', function() {
		var PASSWORD = 'abc123';
		var newUser = new User({
			username: TEST_USER_NAME,
			password: PASSWORD
		});
		return newUser.save()
			.then(function(res) {
				assert.equal(res.username, TEST_USER_NAME, 'username did not match');
				assert.equal(res.password, PASSWORD, 'password did not match');
			});
	});

	it('should get users successfully', function() {
		return User.find()
			.then(function(res) {
				assert.equal(res[0].username, TEST_USER_NAME, 'username did not match');
			})
	});

	it('should get the user successfully by id', function() {
		return User.find()
			.then(function(res) {
				return res[0]._id;
			})
			.then(function(res) {
				return User.findById(res);
			})
			.then(function(res) {
				assert.equal(res.username, TEST_USER_NAME, 'username did not match');
			});
	});

	it('should delete the user successfully', function() {
		return User.find()
			.then(function(res) {
				return res[0]._id;
			})
			.then(function(res) {
				return User.findByIdAndRemove(res);
			})
			.then(function() {
				return User.find();
			})
			.then(function(res) {
				assert.notOk(res.length, 'response was not emtpy');
			})
	});
});