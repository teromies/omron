var express = require('express');
var request = require("supertest-as-promised");

var app = require('../app.js');
var User = require('../models/user-model.js');

var TEST_USER = {
	username: 'TEST_USERNAME',
	password: '12345678'
}

describe('API test', () => {

	before(() => {
		return createUser(TEST_USER);
	});

	after(() => {
		return removeUser(TEST_USER)
	});

	it('should not require authentication in route /', () => {
		return request(app)
			.get('/')
			.expect(404);
	});

	it('should require authentication in route /api/something', () => {
		return request(app)
			.get('/api/something')
			.expect(401);
	});

	it('should return 401 when trying to login without parameters', () => {
		return request(app)
			.post('/login')
			.expect(401);
	});

	it('should return correct response when username is not found', () => {
		return request(app)
			.post('/login')
			.send({username:'user'})
			.expect(401, {
				success: false,
				message: 'Wrong username!'
			});
	});

	it.only('should return correct response when password is wrong', () => {
		return request(app)
				.post('/login')
				//.set('Content-type','appilcation/json')
				.send({username: TEST_USER.username})
				.expect(401, {
					success: false,
					message: 'Wrong password!'
				});				
	});
});

function createUser(user) {
	var newUser = User(user);
	return newUser.save({
		username: user.username,
		password: user.password
	})
	.then((created) => {
		TEST_USER = created;
	})
}

function removeUser(user) {
	return	User.findByIdAndRemove(TEST_USER._id).exec();
}